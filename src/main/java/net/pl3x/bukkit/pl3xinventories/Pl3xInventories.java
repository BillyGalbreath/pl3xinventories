package net.pl3x.bukkit.pl3xinventories;

import net.pl3x.bukkit.pl3xinventories.configuration.Config;
import net.pl3x.bukkit.pl3xinventories.configuration.Lang;
import net.pl3x.bukkit.pl3xinventories.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xinventories.listener.PlayerListener;
import net.pl3x.bukkit.pl3xinventories.task.AutoSave;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xInventories extends JavaPlugin {
    private AutoSave autoSave;

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        autoSave = new AutoSave();
        autoSave.runTaskTimer(this, 1200, 1200);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        autoSave.cancel();
        autoSave.run();

        PlayerConfig.removeConfigs();

        Logger.info(getName() + " disabled.");
    }

    public static Pl3xInventories getPlugin() {
        return Pl3xInventories.getPlugin(Pl3xInventories.class);
    }
}
