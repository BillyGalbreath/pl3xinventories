package net.pl3x.bukkit.pl3xinventories.task;

import net.pl3x.bukkit.pl3xinventories.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class AutoSave extends BukkitRunnable {
    @Override
    public void run() {
        for (Player online : Bukkit.getServer().getOnlinePlayers()) {
            PlayerConfig.getConfig(online).setInventory(online.getGameMode().name(), online.getInventory().getContents());
        }
    }
}
