package net.pl3x.bukkit.pl3xinventories.listener;

import net.pl3x.bukkit.pl3xinventories.configuration.Config;
import net.pl3x.bukkit.pl3xinventories.configuration.Lang;
import net.pl3x.bukkit.pl3xinventories.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

public class PlayerListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInvSeeCreative(PlayerCommandPreprocessEvent event) {
        if (!(event.getMessage().toLowerCase().startsWith("/invsee") ||
                event.getMessage().toLowerCase().startsWith("/einvsee"))) {
            return;
        }

        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.CREATIVE) {
            event.setCancelled(true);
            Lang.send(player, Lang.CREATIVE_INVSEE_SELF);
            return;
        }

        String[] args = event.getMessage().split(" ");
        if (args.length == 0) {
            return;
        }
        //noinspection deprecation (fucking bukkit)
        Player target = Bukkit.getPlayer(args[1]);
        if (target == null) {
            return;
        }
        if (target.getGameMode() == GameMode.CREATIVE) {
            event.setCancelled(true);
            Lang.send(player, Lang.CREATIVE_INVSEE_TARGET);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PlayerConfig pConfig = PlayerConfig.getConfig(player);

        // load current
        ItemStack[] storedInventory = pConfig.getInventory(player.getGameMode().name());
        if (storedInventory != null) {
            player.getInventory().setContents(storedInventory);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        PlayerConfig pConfig = PlayerConfig.getConfig(player);

        // store current
        pConfig.setInventory(player.getGameMode().name(), player.getInventory().getContents());
        pConfig.discard();
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerChangeGameMode(PlayerGameModeChangeEvent event) {
        Player player = event.getPlayer();
        PlayerConfig pConfig = PlayerConfig.getConfig(player);

        // store current
        pConfig.setInventory(player.getGameMode().name(), player.getInventory().getContents());
        // ! do not discard. leave in memory

        // wipe inventory
        player.getInventory().clear();

        // load stored inventory
        ItemStack[] storedInventory = pConfig.getInventory(event.getNewGameMode().name());
        if (storedInventory != null) {
            player.getInventory().setContents(storedInventory);
        }

        // remove active potion effects
        if (Config.REMOVE_POTION_EFFECTS) {
            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onOpenEnderchest(InventoryOpenEvent event) {
        if (!Config.DISABLE_CREATIVE_ENDERCHEST) {
            return; // creative enderchests allowed
        }

        if (event.getView().getTopInventory().getType() != InventoryType.ENDER_CHEST) {
            return; // not opening an enderchest
        }

        HumanEntity player = event.getPlayer();
        if (player.getGameMode() != GameMode.CREATIVE) {
            return; // not in creative mode
        }

        if (player.hasPermission("creative.enderchest.bypass")) {
            return; // has permission bypass
        }

        // cancel opening creative enderchest
        Lang.send(player, Lang.CREATIVE_ENDERCHEST_DISABLED);
        event.setCancelled(true);
    }

    public void onXpOrbPickup(PlayerExpChangeEvent event) {
        if (!Config.DISABLE_XP_CREATIVE_PICKUP) {
            return; // allowed
        }

        if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
            return; // not in creative
        }

        if (event.getAmount() < 0) {
            return; // not going to stop xp decrease
        }

        event.setAmount(0);
    }
}
