package net.pl3x.bukkit.pl3xinventories.configuration;

import net.pl3x.bukkit.pl3xinventories.Pl3xInventories;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Lang {
    public static String CREATIVE_ENDERCHEST_DISABLED;
    public static String CREATIVE_INVSEE_SELF;
    public static String CREATIVE_INVSEE_TARGET;

    public static void reload() {
        Pl3xInventories plugin = Pl3xInventories.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        CREATIVE_ENDERCHEST_DISABLED = config.getString("creative-enderchest-disabled", "&4Creative enderchests are disabled!");
        CREATIVE_INVSEE_SELF = config.getString("creative-invsee-self", "&4You cannot /invsee while in creative mode!");
        CREATIVE_INVSEE_TARGET = config.getString("creative-invsee-target", "&4You cannot /invsee while target is in creative mode!");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
