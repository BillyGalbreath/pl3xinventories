package net.pl3x.bukkit.pl3xinventories.configuration;

import net.pl3x.bukkit.pl3xinventories.Pl3xInventories;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static boolean DISABLE_CREATIVE_ENDERCHEST = true;
    public static boolean REMOVE_POTION_EFFECTS = true;
    public static boolean DISABLE_XP_CREATIVE_PICKUP = true;

    public static void reload() {
        Pl3xInventories plugin = Pl3xInventories.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        DISABLE_CREATIVE_ENDERCHEST = config.getBoolean("disable-creative-enderchest", true);
        REMOVE_POTION_EFFECTS = config.getBoolean("remove-potion-effects", true);
        DISABLE_XP_CREATIVE_PICKUP = config.getBoolean("disable-xp-orb-creative-pickup", true);
    }
}
