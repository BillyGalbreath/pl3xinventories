package net.pl3x.bukkit.pl3xinventories.configuration;

import net.pl3x.bukkit.pl3xinventories.Logger;
import net.pl3x.bukkit.pl3xinventories.Pl3xInventories;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerConfig extends YamlConfiguration {
    private static final Map<UUID, PlayerConfig> configs = new HashMap<>();

    public static PlayerConfig getConfig(Player player) {
        return getConfig(player.getUniqueId());
    }

    public static PlayerConfig getConfig(UUID uuid) {
        synchronized (configs) {
            if (configs.containsKey(uuid)) {
                return configs.get(uuid);
            }
            PlayerConfig config = new PlayerConfig(uuid);
            configs.put(uuid, config);
            return config;
        }
    }

    public static void removeConfigs() {
        Collection<PlayerConfig> oldConfs = new ArrayList<>(configs.values());
        synchronized (configs) {
            oldConfs.forEach(PlayerConfig::discard);
        }
    }

    private File file = null;
    private final Object saveLock = new Object();
    private final UUID uuid;
    private ItemStack[] inventoryCache;

    private PlayerConfig(UUID uuid) {
        super();
        file = new File(Pl3xInventories.getPlugin().getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml");
        this.uuid = uuid;
        reload();
    }

    private void reload() {
        synchronized (saveLock) {
            try {
                load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    public void discard() {
        synchronized (configs) {
            configs.remove(uuid);
        }
    }

    public void setInventory(String gamemode, ItemStack[] inventory) {
        if (Arrays.equals(inventoryCache, inventory)) {
            return; // no change in inventory since last save
        }

        set(gamemode + ".size", inventory.length);
        for (int i = 0; i < inventory.length; i++) {
            ItemStack stack = inventory[i];
            set(gamemode + "." + i, stack);
        }
        save();
        Logger.debug("Saved " + gamemode + " inventory. (" + uuid + ")");
        inventoryCache = inventory.clone();
    }

    public ItemStack[] getInventory(String gamemode) {
        int size = getInt(gamemode + ".size", 0);

        if (size == 0) {
            Logger.debug(gamemode + " inventory doesnt exist. (" + uuid + ")");
            return null;
        }

        ItemStack[] inventory = new ItemStack[size];
        for (int i = 0; i < size; i++) {
            Object obj = get(gamemode + "." + i);
            if (obj == null) {
                continue;
            }
            if (!(obj instanceof ItemStack)) {
                Logger.debug(gamemode + " item " + i + " is corrupt. (" + uuid + ")");
                return null;
            }
            inventory[i] = (ItemStack) obj;
        }

        Logger.debug("Loaded " + gamemode + " inventory. (" + uuid + ")");
        return inventoryCache = inventory.clone();
    }
}
